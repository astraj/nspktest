import aiohttp
import asyncio
import async_timeout
import json

async def myrequest(session, url, params):
    with async_timeout.timeout(4):
        async with session.get(url, params=params) as response:
            statuscode = response.status
            # обработка кода сообщений
            if (statuscode != 200):
                print ("Error Code ")
                response = json.dumps({"error": statuscode})
            return await response.text()

async def main(method, params, prefix=''):
    async with aiohttp.ClientSession() as session:
        url = prefix + method
        # проверка, что получили ответ на запрос
        try:
            request_json = await myrequest(session, url, params)
        except aiohttp.client_exceptions.ClientConnectorError:
            print("No connection")
            return
        # проверка, что получили в ответе JSON или валидный json
        try:
            json.loads(request_json)
        except ValueError:
            print("Invalid JSON")
            return
        print (request_json)



loop = asyncio.get_event_loop()

params = {
    'user_ids': 1,
    'fields':'city'
}
params2 = {
     'group_ids': 29906802 ,
     'count':'20'
 }
prefix = 'https://api.vk.com/method/'
# создаем два асинхронных запросов для демонстрации
tasks = [loop.create_task(main('users.get', params, prefix)),
        loop.create_task(main('http://python.org', params2))]
wait_tasks = asyncio.wait(tasks)
loop.run_until_complete(wait_tasks)
loop.close()